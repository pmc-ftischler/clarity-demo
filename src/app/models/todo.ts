export interface Todo {
    title: string;
    description?: string;
    createdAt: Date;
    done: boolean;
    doneAt?: Date;
}
