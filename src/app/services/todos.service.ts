import { Injectable } from '@angular/core';
import { Todo } from '../models/todo';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

@Injectable()
export class TodosService {
  private todos: Todo[] = [{
      title: 'test',
      description: 'Test',
      createdAt: new Date(),
      done: true
  }];

  public getTodos(): Observable<Todo[]> {
      return of(this.todos);
  }

  public addTodo(todo: Todo) {
      this.todos = [...this.todos, todo];
  }

  public removeTodo(todo: Todo) {
      this.todos.splice(this.todos.indexOf(todo), 1);
  }
}
