import { ChangeDetectionStrategy, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Todo } from '../models/todo';
import { TodosService } from '../services/todos.service';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

@Component({
    styleUrls: ['./home.component.scss'],
    templateUrl: './home.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    preserveWhitespaces: false
})
export class HomeComponent implements OnInit {
    public todos$: Observable<Todo[]>;
    public modalOpened = false;
    public todoFormGroup: FormGroup = new FormGroup({
        title: new FormControl('', Validators.required),
        description: new FormControl('')
    });

    constructor(private todosService: TodosService) {
    }

    ngOnInit() {
        this.loadTodos();
    }

    public toggleModal(): void {
        this.modalOpened = !this.modalOpened;

        if (this.modalOpened) {
            this.todoFormGroup.reset();
        }
    }

    public addTodo(): void {
        if (this.todoFormGroup.invalid) {
            return;
        }
        const todo: Todo = {
            title: this.title.value,
            description: this.description.value,
            done: false,
            createdAt: new Date()
        };

        this.todosService.addTodo(todo);
        this.loadTodos();
        this.toggleModal();
        this.todoFormGroup.reset();
    }

    public removeTodo(todo: Todo): void {
        this.todosService.removeTodo(todo);
    }

    public toggleDone(todo: Todo): void {
        todo.done = !todo.done;
    }

    public changeTitle(title: string, todo: Todo): void {
        todo.title = title;
    }

    public changeDescription(description: string, todo: Todo): void {
        todo.description = description;
    }

    public get title(): AbstractControl {
        return this.todoFormGroup.get('title');
    }

    public get description(): AbstractControl {
        return this.todoFormGroup.get('description');
    }

    private loadTodos(): void {
        this.todos$ = this.todosService.getTodos();
    }
}
